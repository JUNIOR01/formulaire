-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 21 juin 2021 à 18:44
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `traitement_de_formulaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `date_naissance` date NOT NULL,
  `numero_tel` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mot_pass` varchar(255) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `date_naissance`, `numero_tel`, `email`, `mot_pass`, `pays`, `sexe`, `photo`) VALUES
(1, 'kapseu', 'bernard', '2021-06-01', 524578965, 'kapseubernard@yahoo.fr', 'hhngngkgkmhmkksk', 'CANADA', 'HOMME', '1.jpg'),
(2, 'tcheuffa', 'landry', '2021-06-01', 524578965, 'tcheuffalandry@yahoo.fr', 'hhnfgcbfgdtbf fkfkf', 'cameroun', 'HOMME', '1.jpg'),
(4, 'dewewe', 'wewe', '2021-06-02', 2147483647, 'wewew@gmail.com', 'hgdghgsdsd', 'CANADA', 'HOMME', 'f1.jpg'),
(6, 'dewewe', 'wewe', '2021-06-02', 2147483647, 'wewdsew@gmail.com', 'twyeyrwsdd', 'CANADA', 'HOMME', 'f1.jpg'),
(7, 'defo', 'fabien', '2021-06-18', 678304578, 'fabiendefo@yahoor', 'vvnm,v,mvnsvnkvnlk', 'cameroun', 'HOMME', 'c2.jpg');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
