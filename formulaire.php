<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>formulaire</title>
	<link rel="stylesheet" type="text/css" href="css/formulaire.css">
	<link rel="stylesheet" type="text/css" href="css/form.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/font-ionicons.mic.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">

</head>

<body style="background-color: #039be5;" >
	<div class="container-fluid">
		<div class="row ">
			<div class="col-md-offset-2 col-md-4 form1">
				<h2 class="ins">Inscrivez vous ici</h2>
				<form method="post" action="formulaire_traitement.php" enctype="multipart/form-data">
					<p class="alert">?? veuillez remplir tous les champs svp ??</p>
					<label>NOM</label>
					<i class="fa fa-user"></i><input type="text" name="nom" required="" class="form-control" placeholder="entrer votre nom" pattern="[a-zA-ZÀ-ÿ]{}" >
					<p class="alert">?? veuillez saisir minimum 4 caracteres ??</p>
					<label>PRENOM</label>
					<i class="fa fa-user"></i><input type="text" name="prenom" required="" class="form-control" placeholder="entrer votre prenom" pattern="[a-zA-ZÀ-ÿ]{}">
					<p class="alert">?? veuillez saisir minimum 4 caracteres ??</p>
					<label>DATE DE NAISSANCE</label>
					<input type="date" name="date_naissance" required="" class="form-control">
					<p class="alert">?? veuillez saisir une date majeure ??</p>
					<label>NUMERO DE TELEPHONE</label>         <i class="fa fa-phone"></i>
					<div class="input-group">	
						<span class="input-group-addon">
							<select class="input-group">
								<option selected="selected" disabled="">+...</option>
								<option>+237</option>
								<option>+33</option>
								<option>+224</option>
								<option>+225</option>
								<option>+32</option>
								<option>+55</option>
								<option>+225</option>
								<option>+226</option>
								<option>AUTRES</option>
							</select>
						</span>
						<input class="form-control" type="text" name="numero_tel" placeholder="veuillez saisir votre numero de telephone" required="" pattern="[0-9]+">
						<p class="alert">veuillez entrer au maximum 17 chiffres </p>
					</div>
					<label>EMAIL</label>
					<i class="fa fa-envelope"></i><input type="email" name="email" required="" class="form-control" placeholder="entrer votre email">
					<p class="alert">?? votre adresse mail n est pas conforme ??</p>
					<label>MOT DE PASSE</label>    <i class="fa fa-unlock-alt" aria-hidden="true"></i>
					<input type="password" name="mot_pass" required="" class="form-control" placeholder="creer votre mot de passe"><br>
					<p class="alert">?? veuillez inserer aumoins 8 caracteres ??</p>
					<label>CHOISISSEZ VOTE PAYS</label>    <i class="fa fa-globe"></i>
					<div class="input-group">
						<span class="input-group-addon">
							<select>
								<option selected="selected" disabled="">CAMEROUN</option>
								<option>COTE D IVOIRE</option>
								<option>GHANA</option>
								<option>NIGERIA</option>
								<option>CANADA</option>
								<option>SENEGAL</option>
								<option>GABON</option>
								<option>FRANCE</option>
								<option>AUTRE....</option>
							</select>
						</span>
						<input class="form-control" type="text" name="pays" placeholder="veuillez saisir le nom de pays" required="">
					</div>
					<label>SEXE</label>
					<input type="radio" name="sexe" value="HOMME" required="" >HOMME
					<input type="radio" name="sexe" required="" value="FEMME">FEMME<br>
					<label for="photo">PHOTO DE PROFIL:</label>
                      <input type="file" name="photo" id="photo" class="form-control" accept="image/*" onchange="loadFile(event)" required="">
                      <div class="col-md-offset-4 col-md-4" style="margin-top: 10px; margin-bottom: 10px; height: 120px; width: 120px; border-radius: 50%;">
                             <img id="pp" style="height: 120px; width: 120px; border-radius: 50%;" />
                      </div><br>
                    <input type="submit" name="envoyer" class="form-control btn btn-info" value="envoyer">
				</form>
				
			</div>
			
		</div>
		
	</div>





	    <script type="text/javascript" src="javascript/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript">
    		var loadFile = function(event) {
       	 	var profil = document.getElementById('pp');
        	profil.src = URL.createObjectURL(event.target.files[0]);
      		};
 		</script>

 		<script type="text/javascript">
 			var nom=document.getElementsByTagName('input')[0];
 			var prenom=document.getElementsByTagName('input')[1];
 			var date=document.getElementsByTagName('input')[2];
 			var mail=document.getElementsByTagName('input')[4];
 			var pass=document.getElementsByTagName('input')[5];

 			var p_alert=document.getElementsByTagName('p')[0];
 			var p_nom=document.getElementsByTagName('p')[1];
 			var p_prenom=document.getElementsByTagName('p')[2];
 			var p_date=document.getElementsByTagName('p')[3];
 			var p_mail=document.getElementsByTagName('p')[5];
 			var p_pass=document.getElementsByTagName('p')[6];
 			var telephone=document.getElementsByTagName('p')[4];


			nom.addEventListener('change', testnom);
			function testnom(){
			console.log(nom.value);
				if(nom.value.length<3){
					p_nom.style.display='block';
					console.log("longeur du nom "+nom.value.length);
					return false;
				}
				else{
					console.log("longeur du nom "+nom.value.length+" ok");
					p_nom.style.display='none';
					nom.style.backgroundColor='rgb(232,240,254)';
					return true;
				}
			}

				prenom.addEventListener('change', testprenom);
			// console.log(prenom);
			function testprenom(){
				if(prenom.value.length<3){
					p_prenom.style.display='block';
					return false;
				}
				else{
					p_prenom.style.display='none';
					prenom.style.backgroundColor='rgb(232,240,254)';
					return true;
				}
			}

				mail.addEventListener('change', testmail);
			// console.log(mail);
			function testmail(){
				var mail_indic='0';
				var mail_format=[],mail_ending=[];
				mail_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com'];
				var mail_ok;
				// console.log('test');
				for(var i=0; i<mail.value.length; i++){
					if(mail.value[i]=='@'){
						mail_indic=mail.value[i];
					}
				}
				// console.log(mail_indic);
				if(mail_indic=='@'){
					mail_ending=mail.value.split('@');
					// console.log(mail_ending);
					for (var i = 0; i < mail_format.length; i++) {
						// console.log(mail_format[i]+' ?= '+mail_ending[1]);
						if(mail_format[i]==mail_ending[1]){
							mail_ok=true;
							break;
						}
						else{
							mail_ok=false;
						}
					}
				}
				else{
					mail_ok=false;
				}

				if(mail_ok==true){
					p_mail.style.display='none';
					mail.style.backgroundColor='rgb(232,240,254)';
					return true;
				}
				else{
					p_mail.style.display='block';
					return false;
				}
			}
			pass.addEventListener('change',testpass);
			function testpass(){
				if (pass.value.length<8) {
					p_pass.style.display='block';
					return false;
				}else{
					p_pass.style.display='none';
					pass.style.backgroundColor='rgb(232,240,254)';
					return true;
				}
			}

 		</script>

</body>
</html>